package gameconsole;

import ee.ttu.idu0075._2015.ws.console.AddGameRequest;
import ee.ttu.idu0075._2015.ws.console.GameType;
import ee.ttu.idu0075._2015.ws.console.GetGameListRequest;
import ee.ttu.idu0075._2015.ws.console.GetGameListResponse;
import ee.ttu.idu0075._2015.ws.console.GetGameRequest;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Rasmus Tomsen
 */
@Path("games")
public class GamesResource {

    @Context
    private UriInfo context;

    public GamesResource() {
    }
    
    @GET
    @Produces("application/json")
    public GetGameListResponse getGameList(@QueryParam("token") String token) throws WrongTokenException {
        GameConsoleWebService service = new GameConsoleWebService();
        GetGameListRequest request = new GetGameListRequest();
        request.setToken(token);
        return service.getGameList(request);
    }
    
    @GET
    @Path("{id : \\d+}")
    @Produces("application/json")
    public GameType getGame(@PathParam("id") String id, @QueryParam("token") String token) throws WrongTokenException, NoSuchGameException{
        GameConsoleWebService service = new GameConsoleWebService();
        GetGameRequest request = new GetGameRequest();
        request.setId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return service.getGame(request);
    }
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public GameType addGame(AddGameRequest request, @QueryParam("token") String token) throws WrongTokenException {
        GameConsoleWebService service = new GameConsoleWebService();
        request.setToken(token);
        return service.addGame(request);
    }
}

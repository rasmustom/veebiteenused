package gameconsole;

import com.sun.xml.ws.developer.SchemaValidation;
import ee.ttu.idu0075._2015.ws.console.AddConsoleGameRequest;
import ee.ttu.idu0075._2015.ws.console.AddConsoleRequest;
import ee.ttu.idu0075._2015.ws.console.AddGameRequest;
import ee.ttu.idu0075._2015.ws.console.ConsoleGameListType;
import ee.ttu.idu0075._2015.ws.console.ConsoleGameType;
import ee.ttu.idu0075._2015.ws.console.ConsoleType;
import ee.ttu.idu0075._2015.ws.console.ConsoleType.ConsoleDevelopers;
import ee.ttu.idu0075._2015.ws.console.GameType;
import ee.ttu.idu0075._2015.ws.console.GameType.GameDevelopers;
import ee.ttu.idu0075._2015.ws.console.GameType.GamePublishers;
import ee.ttu.idu0075._2015.ws.console.GameType.Genres;
import ee.ttu.idu0075._2015.ws.console.GetConsoleGameListRequest;
import ee.ttu.idu0075._2015.ws.console.GetConsoleListRequest;
import ee.ttu.idu0075._2015.ws.console.GetConsoleListResponse;
import ee.ttu.idu0075._2015.ws.console.GetConsoleRequest;
import ee.ttu.idu0075._2015.ws.console.GetGameListRequest;
import ee.ttu.idu0075._2015.ws.console.GetGameListResponse;
import ee.ttu.idu0075._2015.ws.console.GetGameRequest;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.jws.WebService;

/**
 *
 * @author Rasmus Tomsen
 */
@SchemaValidation
@WebService(serviceName = "ConsoleService", portName = "ConsolePort", endpointInterface = "ee.ttu.idu0075._2015.ws.console.ConsolePortType", targetNamespace = "http://www.ttu.ee/idu0075/2015/ws/console", wsdlLocation = "WEB-INF/wsdl/GameConsoleWebService/GameConsoleService.wsdl")
public class GameConsoleWebService {
    static int nextGameId = 1;
    static int nextConsoleId = 1;
    static List<GameType> listOfGames = new ArrayList<>();
    static List<ConsoleType> listOfConsoles = new ArrayList<>();
    private final String currentToken = "salajane";
    static HashMap<String, ConsoleType> consoleMap = new HashMap<>();
    static HashMap<String, GameType> gameMap = new HashMap<>();
    static HashMap<BigInteger, List<BigInteger>> consoleGameMap = new HashMap<>();


    public GameType getGame(GetGameRequest parameter) throws WrongTokenException, NoSuchGameException {
        GameType gameType = null;
        if (parameter.getToken().equalsIgnoreCase(currentToken)) {
            for (GameType game : listOfGames) {
                if (game.getId().equals(parameter.getId())) {
                    gameType = game;
                }
            }
        } else {
            throw new WrongTokenException("Wrong token!");
        }
        if (gameType == null) {
            throw new NoSuchGameException("Game does not exist!");
        }
        return gameType;
    }

    public GameType addGame(AddGameRequest parameter) throws WrongTokenException {
        GameType gameType = new GameType();
        if (parameter.getToken().equalsIgnoreCase(currentToken)) {
            if (!gameMap.containsKey(currentToken + parameter.getGameCode())) {
                gameType.setGameCode(parameter.getGameCode());
                gameType.setGameName(parameter.getGameName());
                if (parameter.getAltName() != null) {
                    gameType.setAltName(parameter.getAltName());
                }
                if (parameter.getGameItemNumber() != null) {
                    gameType.setGameItemNumber(parameter.getGameItemNumber());
                }
                if (parameter.getGameDeveloper() != null) {
                    GameDevelopers devs = new GameDevelopers();
                    for (String dev : parameter.getGameDeveloper()) {
                        devs.getGameDeveloper().add(dev);
                    }
                    gameType.setGameDevelopers(devs);
                }
                if (parameter.getGamePublisher() != null) {
                    GamePublishers pubs = new GamePublishers();
                    for (String pub : parameter.getGamePublisher()) {
                        pubs.getGamePublisher().add(pub);
                    }
                    gameType.setGamePublishers(pubs);
                }
                if (parameter.getRating() != null) {
                    gameType.setRating(parameter.getRating());
                }
                if (parameter.getGameReleaseDate() != null) {
                    gameType.setGameReleaseDate(parameter.getGameReleaseDate());
                }
                if (parameter.getDescription() != null) {
                    gameType.setDescription(parameter.getDescription());
                }
                if (parameter.getGenre() != null) {
                    Genres genres = new Genres();
                    for (String genre : parameter.getGenre()) {
                        genres.getGenre().add(genre);
                    }
                    gameType.setGenres(genres);
                }
                gameType.setGameRegionCode(parameter.getGameRegionCode());
                gameType.setId(BigInteger.valueOf(nextGameId++));
                listOfGames.add(gameType);
                gameMap.put(currentToken + parameter.getGameCode(), gameType);
            } else {
                gameType = gameMap.get(currentToken + parameter.getGameCode());
            }
        } else {
            throw new WrongTokenException("Wrong token!");
        }
        return gameType;
    }

    public GetGameListResponse getGameList(GetGameListRequest parameter) throws WrongTokenException {
        GetGameListResponse response = new GetGameListResponse();
        if (parameter.getToken().equalsIgnoreCase(currentToken)) {
            for (GameType gameType : listOfGames) {
                response.getGame().add(gameType);
            }
        } else {
            throw new WrongTokenException("Wrong token!");
        }
        return response;
    }

    public ConsoleType getConsole(GetConsoleRequest parameter) throws WrongTokenException, NoSuchConsoleException {
        ConsoleType consoleType = null;
        if (parameter.getToken().equalsIgnoreCase(currentToken)) {
            for (ConsoleType console : listOfConsoles) {
                if (console.getId().equals(parameter.getId())) {
                    consoleType = console;
                }
            }
        } else {
            throw new WrongTokenException("Wrong token!");
        }
        if (consoleType == null) {
            throw new NoSuchConsoleException("Console does not exist!");
        }
        return consoleType;
    }

    public ConsoleType addConsole(AddConsoleRequest parameter) throws WrongTokenException {
        ConsoleType consoleType = new ConsoleType();
        System.out.println("Siia jouab");
        if (parameter.getToken().equalsIgnoreCase(currentToken)) {
            if (!consoleMap.containsKey(currentToken + parameter.getConsoleCode())) {
                ConsoleGameListType consoleGameListType = new ConsoleGameListType();
                consoleType.setConsoleCode(parameter.getConsoleCode());
                if (parameter.getConsoleDeveloper() != null) {
                    ConsoleDevelopers devs = new ConsoleDevelopers();
                    for (String dev : parameter.getConsoleDeveloper()) {
                        devs.getConsoleDeveloper().add(dev);
                    }
                    consoleType.setConsoleDevelopers(devs);
                }
                consoleType.setConsoleGameList(consoleGameListType);
                if (parameter.getConsoleItemNumber() != null) {
                    consoleType.setConsoleItemNumber(parameter.getConsoleItemNumber());
                }
                consoleType.setConsoleName(parameter.getConsoleName());
                consoleType.setConsoleRegionCode(parameter.getConsoleRegionCode());
                if (parameter.getConsoleReleaseDate() != null) {
                    consoleType.setConsoleReleaseDate(parameter.getConsoleReleaseDate());
                }
                consoleType.setId(BigInteger.valueOf(nextConsoleId++));
                listOfConsoles.add(consoleType);
                consoleMap.put(currentToken + parameter.getConsoleCode(), consoleType);
            } else {
                consoleType = consoleMap.get(currentToken + parameter.getConsoleCode());
            }
        } else {
            throw new WrongTokenException("Wrong token!");
        }
        return consoleType;
    }
    
    public GetConsoleListResponse getConsoleList(GetConsoleListRequest parameter) throws WrongTokenException {
        GetConsoleListResponse response = new GetConsoleListResponse();
        if (parameter.getToken().equalsIgnoreCase(currentToken)) {
            for (ConsoleType console : listOfConsoles) {
                ConsoleGameListType consoleGameList = console.getConsoleGameList();
                boolean hasGamesReal = !console.getConsoleGameList().getConsoleGame().isEmpty();
                String consoleName = console.getConsoleName();
                String consoleRegCode = console.getConsoleRegionCode();
                
                boolean correctRelGames = false;
                boolean correctName = false;
                boolean correctRegCode = false;
                
                if (parameter.getHasRelatedGames() != null) {
                    boolean hasGamesRequest = parameter.getHasRelatedGames().equalsIgnoreCase("yes");
                    if (((consoleGameList != null && hasGamesRequest && hasGamesReal) 
                                    || (!hasGamesRequest && (consoleGameList == null || !hasGamesReal)))) {
                        correctRelGames = true;
                    }
                } else {
                    correctRelGames = true;
                }
                
                if (parameter.getConsoleName() != null) {
                    String parName = parameter.getConsoleName();
                    if (parName.equalsIgnoreCase(consoleName)) {
                        correctName = true;
                    }
                } else {
                    correctName = true;
                }
                
                if (parameter.getConsoleRegionCode() != null) {
                    String parRegCode = parameter.getConsoleRegionCode();
                    if (parRegCode.equalsIgnoreCase(consoleRegCode)) {
                        correctRegCode = true;
                    }
                } else {
                    correctRegCode = true;
                }
                
                if (correctRelGames && correctName && correctRegCode) {
                    response.getConsole().add(console);
                }
            }
        } else {
            throw new WrongTokenException("Wrong token!");
        }
        return response;
    }

    public ConsoleGameListType getConsoleGameList(GetConsoleGameListRequest parameter) throws WrongTokenException, NoSuchConsoleException {
        ConsoleGameListType consoleGameListType = null;
        if (parameter.getToken().equalsIgnoreCase(currentToken)) {
            for(ConsoleType console : listOfConsoles) {
                if (console.getId().equals(parameter.getConsoleId())) {
                    consoleGameListType = console.getConsoleGameList();
                }
            }
        } else {
            throw new WrongTokenException("Wrong token!");
        }
        if (consoleGameListType == null) {
            throw new NoSuchConsoleException("Console does not exist!");
        }
        return consoleGameListType;
    }

    public ConsoleGameType addConsoleGame(AddConsoleGameRequest parameter) throws WrongTokenException {
        ConsoleGameType consoleGameType = new ConsoleGameType();
        ConsoleType consoleType = new ConsoleType();
        if (parameter.getToken().equalsIgnoreCase(currentToken)) {
            for (ConsoleType console : listOfConsoles) {
                if (console.getId().equals(parameter.getConsoleId())) {
                    consoleType = console;
                }
            }
            for (GameType game : listOfGames) {
                if (game.getId().equals(parameter.getGameId())) {
                    consoleGameType.setGame(game);
                    if (!consoleGameMap.containsKey(consoleType.getId()) ||
                    !consoleGameMap.get(consoleType.getId()).contains(parameter.getGameId())) {
                        consoleType.getConsoleGameList().getConsoleGame().add(consoleGameType);
                        if (consoleGameMap.containsKey(consoleType.getId())) {
                            consoleGameMap.get(consoleType.getId()).add(game.getId());
                        } else {
                            List<BigInteger> gameList = new ArrayList<>();
                            gameList.add(game.getId());
                            consoleGameMap.put(consoleType.getId(), gameList);
                        }
                    }
                }
            }
        } else {
            throw new WrongTokenException("Wrong token!");
        }
        return consoleGameType;
    }
    
}

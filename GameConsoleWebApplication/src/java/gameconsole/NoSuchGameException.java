package gameconsole;

/**
 *
 * @author Rasmus Tomsen
 */
public class NoSuchGameException extends Exception {
    
    public NoSuchGameException(String message) {
        super(message);
    }
    
    public NoSuchGameException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

package gameconsole;

import ee.ttu.idu0075._2015.ws.console.AddConsoleGameRequest;
import ee.ttu.idu0075._2015.ws.console.ConsoleGameListType;
import ee.ttu.idu0075._2015.ws.console.ConsoleGameType;
import ee.ttu.idu0075._2015.ws.console.GetConsoleGameListRequest;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Rasmus Tomsen
 */
@Path("consoleGames")
public class ConsoleGamesResource {

    @Context
    private UriInfo context;

    public ConsoleGamesResource() {
    }
    
    @GET
    @Path("{consoleId : \\d+}")
    @Produces("application/json")
    public ConsoleGameListType getConsoleGameList(@PathParam("consoleId") String consoleId, @QueryParam("token") String token) throws WrongTokenException, NoSuchConsoleException {
        GameConsoleWebService service = new GameConsoleWebService();
        GetConsoleGameListRequest request = new GetConsoleGameListRequest();
        request.setConsoleId(BigInteger.valueOf(Integer.parseInt(consoleId)));
        request.setToken(token);
        return service.getConsoleGameList(request);
    }
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public ConsoleGameType addConsoleGame(AddConsoleGameRequest request, @QueryParam("token") String token) throws WrongTokenException {
        GameConsoleWebService service = new GameConsoleWebService();
        request.setToken(token);
        return service.addConsoleGame(request);
    }
}

package gameconsole;

import ee.ttu.idu0075._2015.ws.console.AddConsoleRequest;
import ee.ttu.idu0075._2015.ws.console.ConsoleType;
import ee.ttu.idu0075._2015.ws.console.GetConsoleListRequest;
import ee.ttu.idu0075._2015.ws.console.GetConsoleListResponse;
import ee.ttu.idu0075._2015.ws.console.GetConsoleRequest;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Rasmus Tomsen
 */
@Path("consoles")
public class ConsolesResource {

    @Context
    private UriInfo context;

    public ConsolesResource() {
    }
    
    @GET
    @Produces("application/json")
    public GetConsoleListResponse getConsoleList(@QueryParam("token") String token, 
            @QueryParam("name") String name,
            @QueryParam("regionCode") String regionCode,
            @QueryParam("hasRelatedGames") String relGames) throws WrongTokenException {
        GameConsoleWebService service = new GameConsoleWebService();
        GetConsoleListRequest request = new GetConsoleListRequest();
        request.setConsoleName(name);
        request.setConsoleRegionCode(regionCode);
        request.setHasRelatedGames(relGames);
        request.setToken(token);
        return service.getConsoleList(request);
    }
    
    @GET
    @Path("{id : \\d+}")
    @Produces("application/json")
    public ConsoleType getConsole(@PathParam("id") String id, 
            @QueryParam("token") String token) throws WrongTokenException, NoSuchConsoleException {
        GameConsoleWebService service = new GameConsoleWebService();
        GetConsoleRequest request = new GetConsoleRequest();
        request.setId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return service.getConsole(request);
    }
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public ConsoleType addConsole(AddConsoleRequest request, @QueryParam("token") String token) throws WrongTokenException {
        GameConsoleWebService service = new GameConsoleWebService();
        request.setToken(token);
        return service.addConsole(request);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameconsole;

import ee.ttu.idu0075._2015.ws.console.AddConsoleGameRequest;
import ee.ttu.idu0075._2015.ws.console.AddConsoleRequest;
import ee.ttu.idu0075._2015.ws.console.AddGameRequest;
import ee.ttu.idu0075._2015.ws.console.ConsoleGameType;
import ee.ttu.idu0075._2015.ws.console.ConsolePortType;
import ee.ttu.idu0075._2015.ws.console.ConsoleService;
import ee.ttu.idu0075._2015.ws.console.ConsoleType;
import ee.ttu.idu0075._2015.ws.console.GameType;
import ee.ttu.idu0075._2015.ws.console.GetConsoleRequest;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Rasmus Tomsen
 */
public class GameConsoleDemo {
    
    public static void main(String[] args) {
        addConsoleDemo();
        addGameDemo();
        addConsoleGameDemo();
        getConsoleDemo();
    }
    
    private static void addConsoleDemo() {
        System.out.println("Add console demo");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter token: ");
        String token = scanner.next();
        if (token.equalsIgnoreCase("salajane")) {
            AddConsoleRequest request = new AddConsoleRequest();
            System.out.println("Enter console name: ");
            String name = scanner.next();
            System.out.println("Enter console code: ");
            String code = scanner.next();
            System.out.println("Enter console item number: ");
            String itemNumber = scanner.next();
            System.out.println("Enter console release date (Format: YYYY-MM-DD): ");
            String date = scanner.next();
            System.out.println("Enter console region code: ");
            String regionCode = scanner.next();
            
            request.setConsoleCode(code);
            request.setConsoleItemNumber(itemNumber);
            request.setConsoleName(name);
            request.setConsoleRegionCode(regionCode);
            XMLGregorianCalendar date2 = getDate(date);
            if (date2 != null) {
                request.setConsoleReleaseDate(date2);
            }
            request.setToken(token);
            ConsoleType console = addConsole(request);
            System.out.println(console.getId() + ". Console: " + console.getConsoleName());
            System.out.println("Console code: " + console.getConsoleCode());
            System.out.println("Console item number: " + console.getConsoleItemNumber());
            System.out.println("Console release date: " + console.getConsoleReleaseDate());
            System.out.println("Console region code: " + console.getConsoleRegionCode());
        } else {
            System.out.println("Wrong Token!");
        }
    }

    private static void getConsoleDemo() {
        System.out.println();
        System.out.println("Get console demo");
        GetConsoleRequest request = new GetConsoleRequest();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter token: ");
        String token = scanner.next();
        if (token.equalsIgnoreCase("salajane")) {
            System.out.println("Enter console ID: ");
            String id = scanner.next();
            
            request.setId(new BigInteger(id));
            request.setToken(token);
            ConsoleType console = getConsole(request);
            System.out.println(console.getId() + ". Console: " + console.getConsoleName());
            System.out.println("Console code: " + console.getConsoleCode());
            System.out.println("Console item number: " + console.getConsoleItemNumber());
            System.out.println("Console release date: " + console.getConsoleReleaseDate());
            System.out.println("Console region code: " + console.getConsoleRegionCode());
            System.out.println("Console game list: " + console.getConsoleGameList().getConsoleGame());
        } else {
            System.out.println("Wrong Token!");
        }
    }
    
    private static void addGameDemo() {
        System.out.println();
        System.out.println("Add game demo");
        AddGameRequest request = new AddGameRequest();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter token: ");
        String token = scanner.next();
        if (token.equalsIgnoreCase("salajane")) {
            System.out.println("Enter game name:");
            String name = scanner.next();
            System.out.println("Enter game alternative name:");
            String altName = scanner.next();
            System.out.println("Enter game description:");
            String desc = scanner.next();
            System.out.println("Enter game code:");
            String code = scanner.next();
            System.out.println("Enter game item number:");
            String itemNr = scanner.next();
            System.out.println("Enter game region code:");
            String regCode = scanner.next();
            System.out.println("Enter game release date (Fromat: YYYY-MM-DD):");
            String date = scanner.next();
            System.out.println("Enter game rating:");
            String rating = scanner.next();
            
            request.setGameName(name);
            request.setAltName(altName);
            request.setGameCode(code);
            request.setDescription(desc);
            request.setGameItemNumber(itemNr);
            request.setGameRegionCode(regCode);
            XMLGregorianCalendar date2 = getDate(date);
            if (date2 != null) {
                request.setGameReleaseDate(date2);
            }
            request.setRating(rating);
            request.setToken(token);
            GameType game = addGame(request);
            System.out.println(game.getId() + ". Game: " + game.getGameName());
            System.out.println("Game Alt name: " + game.getAltName());
            System.out.println("Game code: " + game.getGameCode());
            System.out.println("Game description: " + game.getDescription());
            System.out.println("Game item number: " + game.getGameItemNumber());
            System.out.println("Game region code: " + game.getGameRegionCode());
            System.out.println("Game release date: " + game.getGameReleaseDate());
            System.out.println("Game rating: " + game.getRating());
        } else {
            System.out.println("Wrong token!");
        }
    }
    
    private static void addConsoleGameDemo() {
        System.out.println();
        System.out.println("Add console game demo");
        AddConsoleGameRequest request = new AddConsoleGameRequest();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter token:");
        String token = scanner.next();
        if (token.equalsIgnoreCase("salajane")) {
            System.out.println("Enter console id:");
            String consoleId = scanner.next();
            System.out.println("Enter game id:");
            String gameId = scanner.next();
            
            request.setConsoleId(new BigInteger(consoleId));
            request.setGameId(new BigInteger(gameId));
            request.setToken(token);
            ConsoleGameType cg = addConsoleGame(request);
            GameType game = cg.getGame();
            System.out.println(game.getId() + ". Game: " + game.getGameName());
            System.out.println("Game Alt name: " + game.getAltName());
            System.out.println("Game code: " + game.getGameCode());
            System.out.println("Game description: " + game.getDescription());
            System.out.println("Game item number: " + game.getGameItemNumber());
            System.out.println("Game region code: " + game.getGameRegionCode());
            System.out.println("Game release date: " + game.getGameReleaseDate());
            System.out.println("Game rating: " + game.getRating());
        } else {
            System.out.println("Wrong token!");
        }
    }
    
    private static XMLGregorianCalendar getDate(String date) {
        GregorianCalendar c = GregorianCalendar.from((LocalDate.parse(date)).atStartOfDay(ZoneId.systemDefault()));
        XMLGregorianCalendar date2 = null;
        try {
            date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(GameConsoleDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date2;
    }
    
    
    private static ConsoleType addConsole(AddConsoleRequest parameter) {
        ConsoleService service = new ConsoleService();
        ConsolePortType port = service.getConsolePort();
        return port.addConsole(parameter);
    }

    private static ConsoleType getConsole(GetConsoleRequest parameter) {
        ConsoleService service = new ConsoleService();
        ConsolePortType port = service.getConsolePort();
        return port.getConsole(parameter);
    }

    private static GameType addGame(AddGameRequest parameter) {
        ConsoleService service = new ConsoleService();
        ConsolePortType port = service.getConsolePort();
        return port.addGame(parameter);
    }

    private static ConsoleGameType addConsoleGame(AddConsoleGameRequest parameter) {
        ConsoleService service = new ConsoleService();
        ConsolePortType port = service.getConsolePort();
        return port.addConsoleGame(parameter);
    }
    
    
    
}

package gameconsole;

/**
 *
 * @author Rasmus Tomsen
 */
public class WrongTokenException extends Exception {
    
    public WrongTokenException(String message) {
        super(message);
    }
    
    public WrongTokenException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

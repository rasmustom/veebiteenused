package gameconsole;

/**
 *
 * @author Rasmus Tomsen
 */
public class NoSuchConsoleException extends Exception {
    
    public NoSuchConsoleException(String message) {
        super(message);
    }
    
    public NoSuchConsoleException(String message, Throwable throwable) {
        super(message, throwable);
    }
}

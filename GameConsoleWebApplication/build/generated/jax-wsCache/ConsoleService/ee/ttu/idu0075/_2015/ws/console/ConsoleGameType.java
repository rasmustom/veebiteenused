
package ee.ttu.idu0075._2015.ws.console;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consoleGameType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consoleGameType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="game" type="{http://www.ttu.ee/idu0075/2015/ws/console}gameType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consoleGameType", propOrder = {
    "game"
})
public class ConsoleGameType {

    @XmlElement(required = true)
    protected GameType game;

    /**
     * Gets the value of the game property.
     * 
     * @return
     *     possible object is
     *     {@link GameType }
     *     
     */
    public GameType getGame() {
        return game;
    }

    /**
     * Sets the value of the game property.
     * 
     * @param value
     *     allowed object is
     *     {@link GameType }
     *     
     */
    public void setGame(GameType value) {
        this.game = value;
    }

}

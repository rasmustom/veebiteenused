
package ee.ttu.idu0075._2015.ws.console;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.ttu.idu0075._2015.ws.console package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetGameResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/console", "getGameResponse");
    private final static QName _AddGameResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/console", "addGameResponse");
    private final static QName _GetConsoleResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/console", "getConsoleResponse");
    private final static QName _AddConsoleResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/console", "addConsoleResponse");
    private final static QName _GetConsoleGameListResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/console", "getConsoleGameListResponse");
    private final static QName _AddConsoleGameResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/console", "addConsoleGameResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.ttu.idu0075._2015.ws.console
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsoleType }
     * 
     */
    public ConsoleType createConsoleType() {
        return new ConsoleType();
    }

    /**
     * Create an instance of {@link GameType }
     * 
     */
    public GameType createGameType() {
        return new GameType();
    }

    /**
     * Create an instance of {@link GetGameRequest }
     * 
     */
    public GetGameRequest createGetGameRequest() {
        return new GetGameRequest();
    }

    /**
     * Create an instance of {@link AddGameRequest }
     * 
     */
    public AddGameRequest createAddGameRequest() {
        return new AddGameRequest();
    }

    /**
     * Create an instance of {@link GetGameListRequest }
     * 
     */
    public GetGameListRequest createGetGameListRequest() {
        return new GetGameListRequest();
    }

    /**
     * Create an instance of {@link GetGameListResponse }
     * 
     */
    public GetGameListResponse createGetGameListResponse() {
        return new GetGameListResponse();
    }

    /**
     * Create an instance of {@link GetConsoleRequest }
     * 
     */
    public GetConsoleRequest createGetConsoleRequest() {
        return new GetConsoleRequest();
    }

    /**
     * Create an instance of {@link AddConsoleRequest }
     * 
     */
    public AddConsoleRequest createAddConsoleRequest() {
        return new AddConsoleRequest();
    }

    /**
     * Create an instance of {@link GetConsoleListRequest }
     * 
     */
    public GetConsoleListRequest createGetConsoleListRequest() {
        return new GetConsoleListRequest();
    }

    /**
     * Create an instance of {@link GetConsoleListResponse }
     * 
     */
    public GetConsoleListResponse createGetConsoleListResponse() {
        return new GetConsoleListResponse();
    }

    /**
     * Create an instance of {@link GetConsoleGameListRequest }
     * 
     */
    public GetConsoleGameListRequest createGetConsoleGameListRequest() {
        return new GetConsoleGameListRequest();
    }

    /**
     * Create an instance of {@link ConsoleGameListType }
     * 
     */
    public ConsoleGameListType createConsoleGameListType() {
        return new ConsoleGameListType();
    }

    /**
     * Create an instance of {@link AddConsoleGameRequest }
     * 
     */
    public AddConsoleGameRequest createAddConsoleGameRequest() {
        return new AddConsoleGameRequest();
    }

    /**
     * Create an instance of {@link ConsoleGameType }
     * 
     */
    public ConsoleGameType createConsoleGameType() {
        return new ConsoleGameType();
    }

    /**
     * Create an instance of {@link ConsoleType.ConsoleDevelopers }
     * 
     */
    public ConsoleType.ConsoleDevelopers createConsoleTypeConsoleDevelopers() {
        return new ConsoleType.ConsoleDevelopers();
    }

    /**
     * Create an instance of {@link GameType.GameDevelopers }
     * 
     */
    public GameType.GameDevelopers createGameTypeGameDevelopers() {
        return new GameType.GameDevelopers();
    }

    /**
     * Create an instance of {@link GameType.GamePublishers }
     * 
     */
    public GameType.GamePublishers createGameTypeGamePublishers() {
        return new GameType.GamePublishers();
    }

    /**
     * Create an instance of {@link GameType.Genres }
     * 
     */
    public GameType.Genres createGameTypeGenres() {
        return new GameType.Genres();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/console", name = "getGameResponse")
    public JAXBElement<GameType> createGetGameResponse(GameType value) {
        return new JAXBElement<GameType>(_GetGameResponse_QNAME, GameType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/console", name = "addGameResponse")
    public JAXBElement<GameType> createAddGameResponse(GameType value) {
        return new JAXBElement<GameType>(_AddGameResponse_QNAME, GameType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsoleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/console", name = "getConsoleResponse")
    public JAXBElement<ConsoleType> createGetConsoleResponse(ConsoleType value) {
        return new JAXBElement<ConsoleType>(_GetConsoleResponse_QNAME, ConsoleType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsoleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/console", name = "addConsoleResponse")
    public JAXBElement<ConsoleType> createAddConsoleResponse(ConsoleType value) {
        return new JAXBElement<ConsoleType>(_AddConsoleResponse_QNAME, ConsoleType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsoleGameListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/console", name = "getConsoleGameListResponse")
    public JAXBElement<ConsoleGameListType> createGetConsoleGameListResponse(ConsoleGameListType value) {
        return new JAXBElement<ConsoleGameListType>(_GetConsoleGameListResponse_QNAME, ConsoleGameListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsoleGameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/console", name = "addConsoleGameResponse")
    public JAXBElement<ConsoleGameType> createAddConsoleGameResponse(ConsoleGameType value) {
        return new JAXBElement<ConsoleGameType>(_AddConsoleGameResponse_QNAME, ConsoleGameType.class, null, value);
    }

}

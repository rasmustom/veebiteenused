
package ee.ttu.idu0075._2015.ws.console;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="consoleCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="consoleName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="consoleReleaseDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="consoleItemNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="consoleRegionCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="consoleDeveloper" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "consoleCode",
    "consoleName",
    "consoleReleaseDate",
    "consoleItemNumber",
    "consoleRegionCode",
    "consoleDeveloper"
})
@XmlRootElement(name = "addConsoleRequest")
public class AddConsoleRequest {

    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected String consoleCode;
    @XmlElement(required = true)
    protected String consoleName;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar consoleReleaseDate;
    protected String consoleItemNumber;
    @XmlElement(required = true)
    protected String consoleRegionCode;
    protected List<String> consoleDeveloper;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the consoleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsoleCode() {
        return consoleCode;
    }

    /**
     * Sets the value of the consoleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsoleCode(String value) {
        this.consoleCode = value;
    }

    /**
     * Gets the value of the consoleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsoleName() {
        return consoleName;
    }

    /**
     * Sets the value of the consoleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsoleName(String value) {
        this.consoleName = value;
    }

    /**
     * Gets the value of the consoleReleaseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getConsoleReleaseDate() {
        return consoleReleaseDate;
    }

    /**
     * Sets the value of the consoleReleaseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setConsoleReleaseDate(XMLGregorianCalendar value) {
        this.consoleReleaseDate = value;
    }

    /**
     * Gets the value of the consoleItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsoleItemNumber() {
        return consoleItemNumber;
    }

    /**
     * Sets the value of the consoleItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsoleItemNumber(String value) {
        this.consoleItemNumber = value;
    }

    /**
     * Gets the value of the consoleRegionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsoleRegionCode() {
        return consoleRegionCode;
    }

    /**
     * Sets the value of the consoleRegionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsoleRegionCode(String value) {
        this.consoleRegionCode = value;
    }

    /**
     * Gets the value of the consoleDeveloper property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the consoleDeveloper property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConsoleDeveloper().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getConsoleDeveloper() {
        if (consoleDeveloper == null) {
            consoleDeveloper = new ArrayList<String>();
        }
        return this.consoleDeveloper;
    }

}
